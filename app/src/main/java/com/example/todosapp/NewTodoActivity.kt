package com.example.todosapp

import android.app.Activity
import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.annotation.RequiresApi
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_new_todo.*
import java.time.LocalDateTime
import java.time.LocalDateTime.now

class NewTodoActivity : AppCompatActivity(), View.OnClickListener {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_todo)

        saveButton.setOnClickListener(this)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onClick(view: View?) {
        when(view?.id){
            R.id.saveButton -> {
                val Intent = Intent()
                val title = titleText.editableText.toString()
                val content = contentText.editableText.toString()
                val isCompleted = isCompleted.isChecked

                try {
                    val Todo = Todo(title, content, isCompleted, now())
                    val json = Gson().toJson(Todo)
                    intent.putExtra(TODO_KEY, json)
                    setResult(Activity.RESULT_OK, intent)
                    finish()

                } catch (ex: Exception) {
                    Toast.makeText(this,"Invalid todo", Toast.LENGTH_SHORT).show()
                }


            }
        }
    }

    companion object {
        val TODO_KEY = "TODO"
    }

}
