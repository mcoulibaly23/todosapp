package com.example.todosapp

import java.time.LocalDateTime

data class Todo (
    val title: String,
    val contents: String,
    val isCompleted: Boolean,
    val current: LocalDateTime
)