package com.example.todosapp

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.google.gson.Gson

import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*

class MainActivity : AppCompatActivity() {

    private var todos: MutableList<Todo> = mutableListOf()

    // Will transition to form
    fun launchTodoScreen() {
        val intent = Intent(this, NewTodoActivity::class.java)
        startActivityForResult(intent, ADD_TODO_REQUEST_CODE)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }

        // Event listener for add button that will allow for transition to add todo form
        addButton.setOnClickListener{ launchTodoScreen() }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when(requestCode) {
            Activity.RESULT_OK -> {
                val json: String? = data?.getStringExtra(NewTodoActivity.TODO_KEY)
                if (json != null) {
                    val todo: Todo? = Gson().fromJson<Todo>(json, Todo::class.java)
                }
            }

            Activity.RESULT_CANCELED -> {
                Toast.makeText(this, "User cancled action", Toast.LENGTH_SHORT).show()
            }
        }
    }



    companion object {
        val ADD_TODO_REQUEST_CODE = 1
    }
}
